# Switcher WebRTC plugin



# Release

Make a new release of this repository:
```
./switcher-plugin-builder/release_switcher_plugin -l switcher-webrtc -r git@gitlab.com:nicobou
```


# Run the test

The first time, prepare a python virtual environment (`venv`).
```
cd src
# Create a python virtual environment
python3 -m venv venv
# activate the environment
source venv/bin/activate
# install dependencies
pip3 install -r requirements.txt
```

The other times:
```
source src/venc/bin/activate
cd build
make test
```

Run the signaling server
----

```
cd src/signaling
python3 ./simple_server.py --disable-ssl --addr 127.0.0.1
```