NEWS
====
Here you will find a high level list of new features and bugfixes for each releases. 

switcher-webrtc 0.0.4 (2023-05-12)
---------------------------

Draft webrtc2 Quiddity:
* increase to Switcher version 3.2.4
* add webrtc-audio-receive-only.js
* connect with claw and use stun/turn properties
* gstreamer pipeline is a non static member
* room and username are now configurable through properties
* glibmainloop is the one from switcher API
* move gmainloop creation and connection to server into start
* set strict_ssl in connect_to_websocket_server_async
* test with jack OK
* fix no transmission when incoming_call_from_peer is invoked
* transmission to webpage working !


switcher-webrtc 0.0.2 (2023-04-18)
---------------------------

* Initial worksj-on-my-computer release

