/*
 * This file is part of switcher.
 *
 * switcher-webrtc is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */


/* The Webrtc2 Quiddity is based on the
   "Demo gstreamer app  for negotiating and streaming a sendrecv audio-only
   webrtc stream to all the peers in a multiparty room." (tag '1.20.1' from the gstreamer mono repo)
   by Nirbheek Chauhan and distributed under the BSD 2-Clause License.
*/ 

#ifndef __SWITCHER_WEBRTC2_PLUGIN_H__
#define __SWITCHER_WEBRTC2_PLUGIN_H__
// Define GST_USE_UNSTABLE_API otherwise compilation fail with the following message:
// "The WebRTC library from gst-plugins-bad is unstable API and may change in future."
// "You can define GST_USE_UNSTABLE_API to avoid this warning."
#define GST_USE_UNSTABLE_API

#include <string>

#include <gst/webrtc/webrtc.h>
#include <json-glib/json-glib.h>
#include <libsoup/soup.h>

#include <switcher/quiddity/quiddity.hpp>
#include <switcher/quiddity/startable.hpp>
#include <switcher/shmdata/follower.hpp>
#include <switcher/gst/pipeliner.hpp>

namespace switcher {
namespace quiddities {
using namespace quiddity;

//! Webrtc Client
/*! 
 This quiddity implements a webrtc client able to communicate
 with multiple peers, that uses shmdatas as input and output
 streams.
*/
class Webrtc2 : public Quiddity, public Startable {
 public:
  Webrtc2(quiddity::Config&&);

 private:
  enum AppState
  {
    APP_STATE_UNKNOWN = 0,
    APP_STATE_ERROR = 1,          /* generic error */
    SERVER_CONNECTING = 1000,
    SERVER_CONNECTION_ERROR,
    SERVER_CONNECTED,             /* Ready to register */
    SERVER_REGISTERING = 2000,
    SERVER_REGISTRATION_ERROR,
    SERVER_REGISTERED,            /* Ready to call a peer */
    SERVER_CLOSED,                /* server connection closed by us or the server */
    ROOM_JOINING = 3000,
    ROOM_JOIN_ERROR,
    ROOM_JOINED,
    ROOM_CALL_NEGOTIATING = 4000, /* negotiating with some or all peers */
    ROOM_CALL_OFFERING,           /* when we're the one sending the offer */
    ROOM_CALL_ANSWERING,          /* when we're the one answering an offer */
    ROOM_CALL_STARTED,            /* in a call with some or all peers */
    ROOM_CALL_STOPPING,
    ROOM_CALL_STOPPED,
    ROOM_CALL_ERROR,
  };
  
  std::unique_ptr<gst::GlibMainLoop> loop_;
  std::unique_ptr<gst::Pipeliner> pipeline_{};
  GstElement *srcbin_{nullptr};
  GList *peers_{nullptr};
  std::string audio_shmpath_{};
  std::string video_shmpath_{};

  SoupWebsocketConnection *ws_conn_{nullptr};
  enum AppState app_state_{APP_STATE_UNKNOWN};
  static gboolean strict_ssl;

  // Switcher things
  static const std::string kConnectionSpec;  //!< Shmdata specifications

  std::string signaling_server_{"wss://localhost:8443"};
  property::prop_id_t signaling_server_id_;

  std::string room_{"noroom"};
  property::prop_id_t room_id_;

  std::string username_{"nouser"};
  property::prop_id_t username_id_;

  std::string stun_server_{"stun://stun.stunprotocol.org:3478"};
  property::prop_id_t stun_server_id_;

  std::string turn_server_{};
  property::prop_id_t turn_server_id_;

  bool on_shmdata_connect(const std::string& shmpath, claw::sfid_t sfid);
  bool on_shmdata_disconnect(claw::sfid_t sfid);

  /*!
    Start the client. Join a room on the signaling server and contact every peer in the room.
  */
  bool start() final;

  /*!
    Stop the client. Disconnect from the signaling server and stop transmitting/receiving media.
  */
  bool stop() final;

  static gint compare_str_glist(gconstpointer a, gconstpointer b);
  static const gchar *find_peer_from_list(GList *peers, const gchar *peer_id);
  bool cleanup_and_quit_loop(const gchar *msg, enum AppState state);
  static gchar *get_string_from_json_object(JsonObject *object);
  static void handle_media_stream(GstPad *pad, GstElement *pipe,
                                  const char *convert_name, const char *sink_name);
  static void on_incoming_decodebin_stream(GstElement *decodebin, GstPad *pad,
                                           gpointer user_data);
  static void on_incoming_stream(GstElement *webrtc, GstPad *pad, gpointer user_data);
  void send_room_peer_msg(const gchar *text, const gchar *peer_id);
  static void send_ice_candidate_message(GstElement *webrtc G_GNUC_UNUSED,
                                  guint mlineindex, gchar *candidate,
                                  const gchar *peer_id);
  void send_room_peer_sdp(GstWebRTCSessionDescription *desc,
                          const gchar *peer_id);
  static void on_offer_created(GstPromise *promise, GstElement *webrtc);
  static void on_negotiation_needed(GstElement *webrtc, const gchar *peer_id);
  void remove_peer_from_pipeline(const gchar *peer_id);
  void add_peer_to_pipeline(const gchar *peer_id);
  void call_peer(const gchar *peer_id);
  void incoming_call_from_peer(const gchar *peer_id);
  bool start_pipeline();
  bool join_room_on_server();
  bool register_with_server();
  static void on_server_closed(SoupWebsocketConnection *conn G_GNUC_UNUSED,
                        gpointer user_data);
  bool do_registration();
  void do_join_room(const gchar *text);
  void handle_error_message(const gchar *msg);
  static void on_answer_created(GstPromise *promise, GstElement *webrtc);
  void handle_sdp_offer(const gchar *peer_id, const gchar *text);
  void handle_sdp_answer(const gchar *peer_id, const gchar *text);
  bool handle_peer_message(const gchar *peer_id, const gchar *msg);
  static void on_server_message(SoupWebsocketConnection *conn,
                         SoupWebsocketDataType type, GBytes *message,
                         gpointer user_data);
  static void on_server_connected(SoupSession *session, GAsyncResult *res,
                                  gpointer user_data);
  void connect_to_websocket_server_async();
  bool check_plugins();
};

SWITCHER_DECLARE_PLUGIN(Webrtc2);

}  // namespace quiddities
}  // namespace switcher
#endif
