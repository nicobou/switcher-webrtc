/*
 * This file is part of switcher-webrtc.
 *
 * switcher-webrtc is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "webrtc2.hpp"
#include <switcher/utils/scope-exit.hpp>

namespace switcher {
namespace quiddities {
SWITCHER_MAKE_QUIDDITY_DOCUMENTATION(
    Webrtc2, "webrtc2", "WebRTC Client",
    "Plugin implementing a simple WebRTC client", "LGPL", "Nicolas Bouillot");

const std::string Webrtc2::kConnectionSpec(R"(
{
"follower":
  [
    {
      "label": "audio",
      "description": "Audio stream for participation in the Webrtc session",
      "can_do": ["audio/x-raw"]
    },
    {
      "label": "video",
      "description": "Video stream for participation in the Webrtc session",
      "can_do": ["video/x-raw"]
    }
  ],
"writer":
  [
    {
      "label": "audio%",
      "description": "Audio stream received from the Webrtc session",
      "can_do": ["audio/x-raw"]
    },
    {
      "label": "video%",
      "description": "Video stream received from the Webrtc session",
      "can_do": [ "video/x-raw" ]
    }
  ]
}
)");

Webrtc2::Webrtc2(quiddity::Config &&conf)
    : Quiddity(
          std::forward<quiddity::Config>(conf),
          {kConnectionSpec,
           [this](const std::string &shmpath, claw::sfid_t sfid) {
             return on_shmdata_connect(shmpath, sfid);
           },
           [this](claw::sfid_t sfid) { return on_shmdata_disconnect(sfid); }}),
      quiddity::Startable(this),
      signaling_server_id_(pmanage<MPtr(&property::PBag::make_string)>(
          "signaling_server",
          [this](const std::string &val) {
            signaling_server_ = val;
            return true;
          },
          [this]() { return signaling_server_; }, "WebSocket Signaling Server",
          "Address of the signaling server,  e.g. wss://127.0.0.1:8443",
          signaling_server_)),
      room_id_(pmanage<MPtr(&property::PBag::make_string)>(
          "room",
          [this](const std::string &val) {
            room_ = val;
            return true;
          },
          [this]() { return room_; }, "Room",
          "Room to join on the signaling server", room_)),
      username_id_(pmanage<MPtr(&property::PBag::make_string)>(
          "username",
          [this](const std::string &val) {
            username_ = val;
            return true;
          },
          [this]() { return username_; }, "Username",
          "Username to use on the server", username_)),
      stun_server_id_(pmanage<MPtr(&property::PBag::make_string)>(
          "stun_server",
          [this](const std::string &val) {
            stun_server_ = val;
            return true;
          },
          [this]() { return stun_server_; }, "STUN server",
          "Address of the STUN server,  e.g. stun://hostname:port",
          stun_server_)),
      turn_server_id_(pmanage<MPtr(&property::PBag::make_string)>(
          "turn_server",
          [this](const std::string &val) {
            turn_server_ = val;
            return true;
          },
          [this]() { return turn_server_; }, "TURN server",
          "Address of the TURN server,  e.g. "
          "turn(s)://username:password@host:port",
          turn_server_)) {
  sw_debug("check plugins");
  if (!check_plugins())
    return;

}

bool Webrtc2::stop() {
  return cleanup_and_quit_loop(nullptr, AppState::APP_STATE_UNKNOWN);
}

bool Webrtc2::start() {
  sw_info("starting (WebRTC Quiddity)");
  sw_debug("create main loop");
  loop_ = std::make_unique<gst::GlibMainLoop>();
  loop_->invoke_in_main_loop([&]() { connect_to_websocket_server_async(); });
  return true;
}

bool Webrtc2::on_shmdata_connect(const std::string &shmpath,
                                 claw::sfid_t sfid) {
  sw_debug("Webrtc::on_shmdata_connect");
  auto label = claw_.get_follower_label(sfid);
  if ("video" == label) {
    video_shmpath_ = shmpath;
  } else if ("audio" == label) {
    audio_shmpath_ = shmpath;
  } else {
    return false;
  }

 return true;
}

bool Webrtc2::on_shmdata_disconnect(claw::sfid_t sfid) {
  sw_debug("Webrtc::on_shmdata_disconnect");
  auto label = claw_.get_follower_label(sfid);
  if ("video" == label) {
    video_shmpath_.clear();
  } else if ("audio" == label) {
    audio_shmpath_.clear();
  } else {
    return false;
  }
  return true;
}

// here
gint Webrtc2::compare_str_glist(gconstpointer a, gconstpointer b) {
  return g_strcmp0(static_cast<const char *>(a), static_cast<const char *>(b));
}

const gchar *Webrtc2::find_peer_from_list(GList *peers, const gchar *peer_id) {
  return static_cast<const char *>(
      (g_list_find_custom(peers, peer_id, compare_str_glist))->data);
}

bool Webrtc2::cleanup_and_quit_loop(const gchar *msg, enum AppState state) {
  sw_debug("cleanup_and_quit_loop");
  if (msg)
    sw_error("{}", msg);
  if (state > 0)
    app_state_= state;

  if (ws_conn_) {
    if (soup_websocket_connection_get_state(ws_conn_) ==
        SOUP_WEBSOCKET_STATE_OPEN)
      /* This will call us again */
      soup_websocket_connection_close(ws_conn_, 1000, "");
    else
      g_object_unref(ws_conn_);
  }

  loop_.reset();
  return true;
}

gchar *Webrtc2::get_string_from_json_object(JsonObject *object) {
  JsonNode *root;
  JsonGenerator *generator;
  gchar *text;

  /* Make it the root node */
  root = json_node_init_object(json_node_alloc(), object);
  generator = json_generator_new();
  json_generator_set_root(generator, root);
  text = json_generator_to_data(generator, nullptr);

  /* Release everything */
  g_object_unref(generator);
  json_node_free(root);
  return text;
}

void Webrtc2::handle_media_stream(GstPad *pad, GstElement *pipe,
                                  const char *convert_name,
                                  const char *sink_name) {
  GstPad *qpad;
  GstElement *q, *conv, *sink;
  GstPadLinkReturn ret;

  q = gst_element_factory_make("queue", nullptr);
  g_assert_nonnull(q);
  conv = gst_element_factory_make(convert_name, nullptr);
  g_assert_nonnull(conv);
  sink = gst_element_factory_make(sink_name, nullptr);
  g_assert_nonnull(sink);
  gst_bin_add_many(GST_BIN(pipe), q, conv, sink, nullptr);
  gst_element_sync_state_with_parent(q);
  gst_element_sync_state_with_parent(conv);
  gst_element_sync_state_with_parent(sink);
  gst_element_link_many(q, conv, sink, nullptr);

  qpad = gst_element_get_static_pad(q, "sink");

  ret = gst_pad_link(pad, qpad);
  g_assert_cmpint(ret, ==, GST_PAD_LINK_OK);
}

void Webrtc2::on_incoming_decodebin_stream(GstElement *decodebin, GstPad *pad,
                                           gpointer user_data) {
  auto context = static_cast<Webrtc2*>(user_data);
  context->sw_debug("on_incoming_decodebin_stream");
  auto pipe = context->pipeline_->get_pipeline();

  GstCaps *caps;
  const gchar *name;

  if (!gst_pad_has_current_caps(pad)) {
    context->sw_error("Pad '{}' has no caps, can't do anything, ignoring",
                      GST_PAD_NAME(pad));
    return;
  }

  caps = gst_pad_get_current_caps(pad);
  name = gst_structure_get_name(gst_caps_get_structure(caps, 0));

  if (g_str_has_prefix(name, "video")) {
    handle_media_stream(pad, pipe, "videoconvert", "autovideosink");
  } else if (g_str_has_prefix(name, "audio")) {
    handle_media_stream(pad, pipe, "audioconvert", "autoaudiosink");
  } else {
    context->sw_error("Unknown pad {}, ignoring", GST_PAD_NAME(pad));
  }
}

void Webrtc2::on_incoming_stream(GstElement *webrtc, GstPad *pad,
                                 gpointer user_data) {
  auto context = static_cast<Webrtc2*>(user_data);
  context->sw_debug("on_incoming_stream");
  auto pipe = context->pipeline_->get_pipeline();
  GstElement *decodebin;
  GstPad *sinkpad;

  if (GST_PAD_DIRECTION(pad) != GST_PAD_SRC)
    return;

  decodebin = gst_element_factory_make("decodebin", nullptr);
  g_signal_connect(decodebin, "pad-added",
                   G_CALLBACK(on_incoming_decodebin_stream), context);
  gst_bin_add(GST_BIN(pipe), decodebin);
  gst_element_sync_state_with_parent(decodebin);

  sinkpad = gst_element_get_static_pad(decodebin, "sink");
  gst_pad_link(pad, sinkpad);
  gst_object_unref(sinkpad);
}

void Webrtc2::send_room_peer_msg(const gchar *text, const gchar *peer_id) {
  sw_debug("send_room_peer_msg");
  gchar *msg;

  msg = g_strdup_printf("ROOM_PEER_MSG %s %s", peer_id, text);
  soup_websocket_connection_send_text(ws_conn_, msg);
  g_free(msg);
}

void Webrtc2::send_ice_candidate_message(GstElement *webrtc, guint mlineindex,
                                         gchar *candidate,
                                         const gchar *peer_id) {
  auto context = static_cast<Webrtc2*>(g_object_get_data(G_OBJECT(webrtc), "switcher_webrtc_self"));
  context->sw_debug("send_ice_candidate_message");
  gchar *text;
  JsonObject *ice, *msg;

  if (context->app_state_ < ROOM_CALL_OFFERING) {
    context->cleanup_and_quit_loop("Can't send ICE, not in call", APP_STATE_ERROR);
    return;
  }

  ice = json_object_new();
  json_object_set_string_member(ice, "candidate", candidate);
  json_object_set_int_member(ice, "sdpMLineIndex", mlineindex);
  msg = json_object_new();
  json_object_set_object_member(msg, "ice", ice);
  text = get_string_from_json_object(msg);
  json_object_unref(msg);

  context->send_room_peer_msg(text, peer_id);
  g_free(text);
}

void Webrtc2::send_room_peer_sdp(GstWebRTCSessionDescription *desc,
                                 const gchar *peer_id) {
  sw_debug("send_room_peer_sdp");
  JsonObject *msg, *sdp;
  const gchar *text, *sdptype;
  gchar *sdptext;

  g_assert_cmpint(app_state_, >=, ROOM_CALL_OFFERING);

  if (desc->type == GST_WEBRTC_SDP_TYPE_OFFER)
    sdptype = "offer";
  else if (desc->type == GST_WEBRTC_SDP_TYPE_ANSWER)
    sdptype = "answer";
  else
    g_assert_not_reached();

  text = gst_sdp_message_as_text(desc->sdp);
  sw_debug("Sending sdp {} to {}:\n{}", sdptype, peer_id, text);

  sdp = json_object_new();
  json_object_set_string_member(sdp, "type", sdptype);
  json_object_set_string_member(sdp, "sdp", text);

  msg = json_object_new();
  json_object_set_object_member(msg, "sdp", sdp);
  sdptext = get_string_from_json_object(msg);
  json_object_unref(msg);

  send_room_peer_msg(sdptext, peer_id);
}

/* Offer created by our pipeline, to be sent to the peer */
void Webrtc2::on_offer_created(GstPromise *promise, GstElement *webrtc) {
  GstWebRTCSessionDescription *offer;
  const GstStructure *reply;


  /* retrieve self */
  auto context = static_cast<Webrtc2*>(g_object_get_data(G_OBJECT(webrtc), "switcher_webrtc_self"));

  gchar *peer_id = gst_element_get_name(webrtc);
  context->sw_debug("on_offer_created for {}", peer_id);
  On_scope_exit { g_free(peer_id); };

  g_assert_cmpint(context->app_state_, ==, ROOM_CALL_OFFERING);

  g_assert_cmpint(gst_promise_wait(promise), ==, GST_PROMISE_RESULT_REPLIED);
  reply = gst_promise_get_reply(promise);
  gst_structure_get(reply, "offer", GST_TYPE_WEBRTC_SESSION_DESCRIPTION, &offer,
                    nullptr);
  gst_promise_unref(promise);

  promise = gst_promise_new();
  g_assert_nonnull(webrtc);
  g_signal_emit_by_name(webrtc, "set-local-description", offer, promise);
  gst_promise_interrupt(promise);
  gst_promise_unref(promise);


  /* Send offer to peer */
  context->send_room_peer_sdp(offer, peer_id);
  gst_webrtc_session_description_free(offer);
}

void Webrtc2::on_negotiation_needed(GstElement *webrtc, const gchar *peer_id) {
  auto context = static_cast<Webrtc2*>(g_object_get_data(G_OBJECT(webrtc), "switcher_webrtc_self"));
  context->sw_debug("on_negotioation_needed");
  GstPromise *promise;

  context->app_state_= ROOM_CALL_OFFERING;
  promise = gst_promise_new_with_change_func(
      (GstPromiseChangeFunc)on_offer_created, webrtc, nullptr);
  g_signal_emit_by_name(webrtc, "create-offer", nullptr, promise);
}

void Webrtc2::remove_peer_from_pipeline(const gchar *peer_id) {
  sw_debug("remove_peer_from_pipeline");
  gchar *qname;
  GstPad *srcpad, *sinkpad;
  GstElement *webrtc, *q, *tee;

  webrtc = gst_bin_get_by_name(GST_BIN(pipeline_->get_pipeline()), peer_id);
  if (!webrtc) {
    sw_error("webrtc bin element not found");
    return;
  }
  gst_bin_remove(GST_BIN(pipeline_->get_pipeline()), webrtc);
  gst_object_unref(webrtc);

  qname = g_strdup_printf("queue-%s", peer_id);
  q = gst_bin_get_by_name(GST_BIN(pipeline_->get_pipeline()), qname);
  g_free(qname);

  sinkpad = gst_element_get_static_pad(q, "sink");
  g_assert_nonnull(sinkpad);
  srcpad = gst_pad_get_peer(sinkpad);
  g_assert_nonnull(srcpad);
  gst_object_unref(sinkpad);

  gst_bin_remove(GST_BIN(pipeline_->get_pipeline()), q);
  gst_object_unref(q);

  tee = gst_bin_get_by_name(GST_BIN(pipeline_->get_pipeline()), "audiotee");
  g_assert_nonnull(tee);
  gst_element_release_request_pad(tee, srcpad);
  gst_object_unref(srcpad);
  gst_object_unref(tee);
}

void Webrtc2::add_peer_to_pipeline(const gchar *peer_id) {
  sw_debug("add_peer_to_pipeline for peer {}", peer_id);
  int ret;
  gchar *tmp;
  GstElement *tee, *webrtc, *q;
  GstPad *srcpad, *sinkpad;

  tmp = g_strdup_printf("queue-%s", peer_id);
  q = gst_element_factory_make("queue", tmp);
  g_free(tmp);
  webrtc = gst_element_factory_make("webrtcbin", peer_id);

  // set stun-server address used for srflx ICE candidates
  g_object_set(G_OBJECT(webrtc),
              "stun-server",
              stun_server_.c_str(),
              nullptr);

  // set turn-server address used for relay ICE candidates
  g_object_set(G_OBJECT(webrtc),
              "turn-server",
              turn_server_.c_str(),
              nullptr);

  
  if (!webrtc) {
    sw_error("webrtc bin element not found");
    return;
  }

  gst_bin_add_many(GST_BIN(srcbin_), q, webrtc, nullptr);

  srcpad = gst_element_get_static_pad(q, "src");
  g_assert_nonnull(srcpad);
  sinkpad = gst_element_request_pad_simple(webrtc, "sink_%u");
  g_assert_nonnull(sinkpad);
  ret = gst_pad_link(srcpad, sinkpad);
  g_assert_cmpint(ret, ==, GST_PAD_LINK_OK);
  gst_object_unref(srcpad);
  gst_object_unref(sinkpad);

  tee = gst_bin_get_by_name(GST_BIN(srcbin_), "audiotee");
  g_assert_nonnull(tee);
  srcpad = gst_element_request_pad_simple(tee, "src_%u");
  g_assert_nonnull(srcpad);
  gst_object_unref(tee);
  sinkpad = gst_element_get_static_pad(q, "sink");
  g_assert_nonnull(sinkpad);
  ret = gst_pad_link(srcpad, sinkpad);
  g_assert_cmpint(ret, ==, GST_PAD_LINK_OK);
  gst_object_unref(srcpad);
  gst_object_unref(sinkpad);

  // attach this to webrtc for use in callbacks 
  g_object_set_data(G_OBJECT(webrtc), "switcher_webrtc_self", this);
  
  /* This is the gstwebrtc entry point where we create the offer and so on. It
   * will be called when the pipeline goes to PLAYING.
   * XXX: We must connect this after webrtcbin has been linked to a source via
   * get_request_pad() and before we go from NULL->READY otherwise webrtcbin
   * will create an SDP offer with no media lines in it. */
  g_signal_connect(webrtc, "on-negotiation-needed",
                   G_CALLBACK(on_negotiation_needed), (gpointer)peer_id);

  /* We need to transmit this ICE candidate to the browser via the websockets
   * signalling server. Incoming ice candidates from the browser need to be
   * added by us too, see on_server_message() */
  g_signal_connect(webrtc, "on-ice-candidate",
                   G_CALLBACK(send_ice_candidate_message), (gpointer)peer_id);
  /* Incoming streams will be exposed via this signal */
  g_signal_connect(webrtc, "pad-added", G_CALLBACK(on_incoming_stream),
                   this);

  /* Set to pipeline branch to PLAYING */
  ret = gst_element_sync_state_with_parent(q);
  g_assert_true(ret);
  ret = gst_element_sync_state_with_parent(webrtc);
  g_assert_true(ret);
}

void Webrtc2::call_peer(const gchar *peer_id) {
  sw_debug("call_peer");
  add_peer_to_pipeline(peer_id);
}

void Webrtc2::incoming_call_from_peer(const gchar *peer_id) {
  sw_debug("incoming_call_from_peer");
  add_peer_to_pipeline(peer_id);
}

bool Webrtc2::start_pipeline() {
  sw_debug("start_pipeline");
  GError *error = nullptr;

  pipeline_ = std::make_unique<gst::Pipeliner>(loop_->get_main_context());
  std::string description =
      "tee name=audiotee ! queue ! fakesink "
      "shmdatasrc copy-buffers=true "
      "socket-path=" +
      audio_shmpath_ +
      " ! audioresample ! "
      "audioconvert ! queue ! "
      "opusenc bandwidth=fullband bitrate=650000 bitrate-type=cbr frame-size=2 "
      "inband-fec=true "
      "! rtpopuspay ! "
      "queue ! application/x-rtp,media=audio,encoding-name=OPUS,payload=96 ! "
      "audiotee. ";

  /* NOTE: webrtcbin currently does not support dynamic addition/removal of
   * streams, so we use a separate webrtcbin for each peer, but all of them are
   * inside the same pipeline. We start by connecting it to a fakesink so that
   * we can preroll early. */
  srcbin_ = gst_parse_bin_from_description_full(description.c_str(),
      false,
      nullptr,
      GST_PARSE_FLAG_NO_SINGLE_ELEMENT_BINS,
      &error);

  if (error) {
    sw_error("Failed to parse launch: {}", error->message);
    g_error_free(error);
    pipeline_.reset();
    return false;
  }

  sw_debug("Starting pipeline, not transmitting yet");
  gst_bin_add(GST_BIN(pipeline_->get_pipeline()), srcbin_);
  pipeline_->play(true);

  return true;
}

bool Webrtc2::join_room_on_server() {
  sw_debug("join room");
  gchar *msg;

  if (soup_websocket_connection_get_state(ws_conn_) != SOUP_WEBSOCKET_STATE_OPEN)
    return false;

  if (room_.empty())
    return false;

  sw_info("Joining room {}", room_);
  app_state_= ROOM_JOINING;
  msg = g_strdup_printf("ROOM %s", room_.c_str());
  soup_websocket_connection_send_text(ws_conn_, msg);
  g_free(msg);
  return true;
}

bool Webrtc2::register_with_server() {
  sw_debug("register with server");
  gchar *hello;

  if (soup_websocket_connection_get_state(ws_conn_) != SOUP_WEBSOCKET_STATE_OPEN)
    return false;

  sw_info("Registering id %s with server", username_.c_str());
  app_state_= SERVER_REGISTERING;

  /* Register with the server with a random integer id. Reply will be received
   * by on_server_message() */
  hello = g_strdup_printf("HELLO %s", username_.c_str());
  soup_websocket_connection_send_text(ws_conn_, hello);
  g_free(hello);

  return true;
}

void Webrtc2::on_server_closed(SoupWebsocketConnection *conn G_GNUC_UNUSED,
                               gpointer user_data) {
  auto *context = static_cast<Webrtc2*>(user_data);
  context->sw_debug("on_server_closed");
  context->app_state_= SERVER_CLOSED;
  context->cleanup_and_quit_loop("Server connection closed", AppState::APP_STATE_UNKNOWN);
}

bool Webrtc2::do_registration() {
  sw_info("do registration with server");
  if (app_state_!= SERVER_REGISTERING) {
    cleanup_and_quit_loop("ERROR: Received HELLO when not registering",
                          APP_STATE_ERROR);
    return false;
  }
  app_state_= SERVER_REGISTERED;
  sw_info("Registered with server");
  /* Ask signalling server that we want to join a room */
  if (!join_room_on_server()) {
    cleanup_and_quit_loop("ERROR: Failed to join room", ROOM_CALL_ERROR);
    return false;
  }
  return true;
}

/*
 * When we join a room, we are responsible for calling by starting negotiation
 * with each peer in it by sending an SDP offer and ICE candidates.
 */
void Webrtc2::do_join_room(const gchar *text) {
  sw_debug("do join room");
  gint ii, len;
  gchar **peer_ids;

  if (app_state_!= ROOM_JOINING) {
    cleanup_and_quit_loop("ERROR: Received ROOM_OK when not calling",
                          ROOM_JOIN_ERROR);
    return;
  }

  app_state_= ROOM_JOINED;
  sw_info("Room joined");
  /* Start recording, but not transmitting */
  if (!start_pipeline()) {
    cleanup_and_quit_loop("ERROR: Failed to start pipeline", ROOM_CALL_ERROR);
    return;
  }

  peer_ids = g_strsplit(text, " ", -1);
  g_assert_cmpstr(peer_ids[0], ==, "ROOM_OK");
  len = g_strv_length(peer_ids);
  /* There are peers in the room already. We need to start negotiation
   * (exchange SDP and ICE candidates) and transmission of media. */
  if (len > 1 && strlen(peer_ids[1]) > 0) {
    sw_info("Found {} peers already in room", len - 1);
    app_state_= ROOM_CALL_OFFERING;
    for (ii = 1; ii < len; ii++) {
      gchar *peer_id = g_strdup(peer_ids[ii]);
      sw_info("Negotiating with peer {}", peer_id);
      /* This might fail asynchronously */
      call_peer(peer_id);
      peers_ = g_list_prepend(peers_, peer_id);
    }
  }

  g_strfreev(peer_ids);
  return;
}

void Webrtc2::handle_error_message(const gchar *msg) {
  sw_debug("handle_error_message");
  switch (app_state_) {
  case SERVER_CONNECTING:
    app_state_= SERVER_CONNECTION_ERROR;
    break;
  case SERVER_REGISTERING:
    app_state_= SERVER_REGISTRATION_ERROR;
    break;
  case ROOM_JOINING:
    app_state_= ROOM_JOIN_ERROR;
    break;
  case ROOM_JOINED:
  case ROOM_CALL_NEGOTIATING:
  case ROOM_CALL_OFFERING:
  case ROOM_CALL_ANSWERING:
    app_state_= ROOM_CALL_ERROR;
    break;
  case ROOM_CALL_STARTED:
  case ROOM_CALL_STOPPING:
  case ROOM_CALL_STOPPED:
    app_state_= ROOM_CALL_ERROR;
    break;
  default:
    app_state_= APP_STATE_ERROR;
  }
  cleanup_and_quit_loop(msg, AppState::APP_STATE_UNKNOWN);
}

void Webrtc2::on_answer_created(GstPromise *promise, GstElement *webrtc) {
  GstWebRTCSessionDescription *answer;
  const GstStructure *reply;


  g_assert_cmpint(gst_promise_wait(promise), ==, GST_PROMISE_RESULT_REPLIED);
  reply = gst_promise_get_reply(promise);
  gst_structure_get(reply, "answer", GST_TYPE_WEBRTC_SESSION_DESCRIPTION,
                    &answer, nullptr);
  gst_promise_unref(promise);

  promise = gst_promise_new();
  g_assert_nonnull(webrtc);
  g_signal_emit_by_name(webrtc, "set-local-description", answer, promise);
  gst_promise_interrupt(promise);
  gst_promise_unref(promise);

  /* retrieve self */
  auto context = static_cast<Webrtc2*>(g_object_get_data(G_OBJECT(webrtc), "switcher_webrtc_self"));
  context->sw_debug("on_answer_created");
  g_assert_cmpint(context->app_state_, ==, ROOM_CALL_ANSWERING);
  
  /* Send offer to peer */
  gchar *peer_id = gst_element_get_name(webrtc);
  context->sw_debug("send offer to {}", peer_id);
  On_scope_exit { g_free(peer_id); };
  context->send_room_peer_sdp(answer, peer_id);
  gst_webrtc_session_description_free(answer);

  context->app_state_= ROOM_CALL_STARTED;
}

void Webrtc2::handle_sdp_offer(const gchar *peer_id, const gchar *text) {
  sw_debug("hanfle_sdp_offer");
  int ret;
  GstPromise *promise;
  GstElement *webrtc;
  GstSDPMessage *sdp;
  GstWebRTCSessionDescription *offer;

  g_assert_cmpint(app_state_, ==, ROOM_CALL_ANSWERING);

  sw_debug("Received offer:\n{}", text);

  ret = gst_sdp_message_new(&sdp);
  g_assert_cmpint(ret, ==, GST_SDP_OK);

  ret = gst_sdp_message_parse_buffer((guint8 *)text, strlen(text), sdp);
  g_assert_cmpint(ret, ==, GST_SDP_OK);

  offer = gst_webrtc_session_description_new(GST_WEBRTC_SDP_TYPE_OFFER, sdp);
  g_assert_nonnull(offer);

  /* Set remote description on our pipeline */
  promise = gst_promise_new();
  webrtc = gst_bin_get_by_name(GST_BIN(pipeline_->get_pipeline()), peer_id);
  g_assert_nonnull(webrtc);
  g_signal_emit_by_name(webrtc, "set-remote-description", offer, promise);
  /* We don't want to be notified when the action is done */
  gst_promise_interrupt(promise);
  gst_promise_unref(promise);

  /* Create an answer that we will send back to the peer */
  promise = gst_promise_new_with_change_func(
      (GstPromiseChangeFunc)on_answer_created, webrtc, nullptr);
  g_signal_emit_by_name(webrtc, "create-answer", nullptr, promise);

  gst_webrtc_session_description_free(offer);
  gst_object_unref(webrtc);
}

void Webrtc2::handle_sdp_answer(const gchar *peer_id, const gchar *text) {
  sw_debug("hanfle_sdp_answer");
  int ret;
  GstPromise *promise;
  GstElement *webrtc;
  GstSDPMessage *sdp;
  GstWebRTCSessionDescription *answer;

  g_assert_cmpint(app_state_, >=, ROOM_CALL_OFFERING);

  sw_debug("Received answer:\n{}", text);

  ret = gst_sdp_message_new(&sdp);
  g_assert_cmpint(ret, ==, GST_SDP_OK);

  ret = gst_sdp_message_parse_buffer((guint8 *)text, strlen(text), sdp);
  g_assert_cmpint(ret, ==, GST_SDP_OK);

  answer = gst_webrtc_session_description_new(GST_WEBRTC_SDP_TYPE_ANSWER, sdp);
  g_assert_nonnull(answer);

  /* Set remote description on our pipeline */
  promise = gst_promise_new();
  webrtc = gst_bin_get_by_name(GST_BIN(pipeline_->get_pipeline()), peer_id);
  g_assert_nonnull(webrtc);
  g_signal_emit_by_name(webrtc, "set-remote-description", answer, promise);
  gst_object_unref(webrtc);
  /* We don't want to be notified when the action is done */
  gst_promise_interrupt(promise);
  gst_promise_unref(promise);
}

bool Webrtc2::handle_peer_message(const gchar *peer_id, const gchar *msg) {
  sw_debug("handle_peer_message");
  JsonNode *root;
  JsonObject *object, *child;
  JsonParser *parser = json_parser_new();
  if (!json_parser_load_from_data(parser, msg, -1, nullptr)) {
    sw_error("Unknown message '{}' from '{}', ignoring", msg, peer_id);
    g_object_unref(parser);
    return false;
  }

  root = json_parser_get_root(parser);
  if (!JSON_NODE_HOLDS_OBJECT(root)) {
    sw_error("Unknown json message '{}' from '{}', ignoring", msg, peer_id);
    g_object_unref(parser);
    return false;
  }

  sw_info("Message from peer {}: {}", peer_id, msg);

  object = json_node_get_object(root);
  /* Check type of JSON message */
  if (json_object_has_member(object, "sdp")) {
    const gchar *text, *sdp_type;

    g_assert_cmpint(app_state_, >=, ROOM_JOINED);

    child = json_object_get_object_member(object, "sdp");

    if (!json_object_has_member(child, "type")) {
      cleanup_and_quit_loop("ERROR: received SDP without 'type'",
                            ROOM_CALL_ERROR);
      return false;
    }

    sdp_type = json_object_get_string_member(child, "type");
    text = json_object_get_string_member(child, "sdp");

    if (g_strcmp0(sdp_type, "offer") == 0) {
      app_state_= ROOM_CALL_ANSWERING;
      incoming_call_from_peer(peer_id);
      handle_sdp_offer(peer_id, text);
    } else if (g_strcmp0(sdp_type, "answer") == 0) {
      g_assert_cmpint(app_state_, >=, ROOM_CALL_OFFERING);
      handle_sdp_answer(peer_id, text);
      app_state_= ROOM_CALL_STARTED;
    } else {
      cleanup_and_quit_loop("ERROR: invalid sdp_type", ROOM_CALL_ERROR);
      return false;
    }
  } else if (json_object_has_member(object, "ice")) {
    GstElement *webrtc;
    const gchar *candidate;
    gint sdpmlineindex;

    child = json_object_get_object_member(object, "ice");
    candidate = json_object_get_string_member(child, "candidate");
    sdpmlineindex = json_object_get_int_member(child, "sdpMLineIndex");

    /* Add ice candidate sent by remote peer */
    webrtc = gst_bin_get_by_name(GST_BIN(pipeline_->get_pipeline()), peer_id);
    g_assert_nonnull(webrtc);
    g_signal_emit_by_name(webrtc, "add-ice-candidate", sdpmlineindex,
                          candidate);
    gst_object_unref(webrtc);
  } else {
    sw_error("Ignoring unknown JSON message:\n{}", msg);
  }
  g_object_unref(parser);
  return true;
}

/* One mega message handler for our asynchronous calling mechanism */
void Webrtc2::on_server_message(SoupWebsocketConnection *conn,
                                SoupWebsocketDataType type, GBytes *message,
                                gpointer user_data) {
  auto *context = static_cast<Webrtc2*>(user_data);

  context->sw_debug("WebRTC: new message from server");
  gchar *text;

  switch (type) {
  case SOUP_WEBSOCKET_DATA_BINARY:
    context->sw_error("Received unknown binary message, ignoring");
    return;
  case SOUP_WEBSOCKET_DATA_TEXT: {
    gsize size;
    const gchar *data =
        static_cast<const gchar *>(g_bytes_get_data(message, &size));
    /* Convert to NULL-terminated string */
    text = g_strndup(data, size);
    break;
  }
  default:
    g_assert_not_reached();
  }

  /* Server has accepted our registration, we are ready to send commands */
  if (g_strcmp0(text, "HELLO") == 0) {
    /* May fail asynchronously */
    context->do_registration();
    /* Room-related message */
  } else if (g_str_has_prefix(text, "ROOM_")) {
    /* Room joined, now we can start negotiation */
    if (g_str_has_prefix(text, "ROOM_OK ")) {
      /* May fail asynchronously */
      context->do_join_room(text);
    } else if (g_str_has_prefix(text, "ROOM_PEER")) {
      gchar **splitm = nullptr;
      const gchar *peer_id;
      /* SDP and ICE, usually */
      if (g_str_has_prefix(text, "ROOM_PEER_MSG")) {
        splitm = g_strsplit(text, " ", 3);
        peer_id = find_peer_from_list(context->peers_, splitm[1]);
        g_assert_nonnull(peer_id);
        /* Could be an offer or an answer, or ICE, or an arbitrary message */
        context->handle_peer_message(peer_id, splitm[2]);
      } else if (g_str_has_prefix(text, "ROOM_PEER_JOINED")) {
        splitm = g_strsplit(text, " ", 2);
        context->peers_ = g_list_prepend(context->peers_, g_strdup(splitm[1]));
        peer_id = find_peer_from_list(context->peers_, splitm[1]);
        g_assert_nonnull(peer_id);
        context->sw_info("Peer {} has joined the room", peer_id);
      } else if (g_str_has_prefix(text, "ROOM_PEER_LEFT")) {
        splitm = g_strsplit(text, " ", 2);
        peer_id = find_peer_from_list(context->peers_, splitm[1]);
        g_assert_nonnull(peer_id);
        context->peers_ = g_list_remove(context->peers_, peer_id);
        context->sw_info("Peer {} has left the room", peer_id);
        context->remove_peer_from_pipeline(peer_id);
        g_free((gchar *)peer_id);
      } else {
        context->sw_warning("Ignoring unknown message {}", text);
      }
      g_strfreev(splitm);
    } else {
      goto err;
    }
    /* Handle errors */
  } else if (g_str_has_prefix(text, "ERROR")) {
    context->handle_error_message(text);
  } else {
    goto err;
  }

out:
  g_free(text);
  return;

err : {
  gchar *err_s = g_strdup_printf("ERROR: unknown message %s", text);
  context->cleanup_and_quit_loop(err_s, AppState::APP_STATE_UNKNOWN);
  g_free(err_s);
  goto out;
}
}

void Webrtc2::on_server_connected(SoupSession *session, GAsyncResult *res,
                                  gpointer user_data) {
  auto *context = static_cast<Webrtc2*>(user_data);
  context->sw_debug("invocation of on_server_connected");
  GError *error = nullptr;

  context->ws_conn_ = soup_session_websocket_connect_finish(session, res, &error);
  if (error) {
    context->cleanup_and_quit_loop(error->message, SERVER_CONNECTION_ERROR);
    g_error_free(error);
    return;
  }

  g_assert_nonnull(context->ws_conn_);

  context->app_state_= SERVER_CONNECTED;
  context->sw_info("Connected to signalling server");

  // cleanup connection message
  
  
  g_signal_connect(context->ws_conn_, "closed", G_CALLBACK(on_server_closed), context);
  g_signal_connect(context->ws_conn_, "message", G_CALLBACK(on_server_message), context);

  /* Register with the server so it knows about us and can accept commands
   * responses from the server will be handled in on_server_message() above */
  context->register_with_server();
}

/*
 * Connect to the signalling server. This is the entrypoint for everything else.
 */
void Webrtc2::connect_to_websocket_server_async() {
  sw_debug("connect websocket server");
  SoupLogger *logger;
  SoupMessage *message;
  SoupSession *session;
  const char *https_aliases[] = {"wss", nullptr};
  bool strict_ssl = true;

  sw_debug("set strict ssl if server is not on localhost");
  /* Don't use strict ssl when running a localhost server, because
   * it's probably a test server with a self-signed certificate */
  {
    GstUri *uri = gst_uri_from_string(signaling_server_.c_str());
    if (g_strcmp0("localhost", gst_uri_get_host(uri)) == 0 ||
        g_strcmp0("127.0.0.1", gst_uri_get_host(uri)) == 0)
      strict_ssl = false;
    gst_uri_unref(uri);
  }

  // The following allows soup calls to use the loop_ main context
  g_main_context_push_thread_default(loop_->get_main_context());

  session = soup_session_new_with_options(
      SOUP_SESSION_SSL_STRICT, strict_ssl, SOUP_SESSION_SSL_USE_SYSTEM_CA_FILE,
      TRUE,
      // SOUP_SESSION_SSL_CA_FILE, "/etc/ssl/certs/ca-bundle.crt",
      SOUP_SESSION_HTTPS_ALIASES, https_aliases, nullptr);

  logger = soup_logger_new(SOUP_LOGGER_LOG_BODY, -1);
  soup_session_add_feature(session, SOUP_SESSION_FEATURE(logger));
  g_object_unref(logger);

  message = soup_message_new(SOUP_METHOD_GET, signaling_server_.c_str());

  sw_info("Connecting to server...");

  /* Once connected, we will register */
  soup_session_websocket_connect_async(session, message, nullptr, nullptr, nullptr,
                                       (GAsyncReadyCallback)on_server_connected,
                                       this);
  
  app_state_= SERVER_CONNECTING;
}

bool Webrtc2::check_plugins() {
  sw_debug("check plugins for the WebRTC quiddity");
  guint i;
  auto ret = true;
  GstRegistry *registry;
  const gchar *needed[] = {"opus", "nice",       "webrtc",       "dtls",
                           "srtp", "rtpmanager", "audiotestsrc", nullptr};

  registry = gst_registry_get();
  for (i = 0; i < g_strv_length((gchar **)needed); i++) {
    GstPlugin *plugin;
    plugin = gst_registry_find_plugin(registry, needed[i]);
    if (!plugin) {
      sw_error("Required gstreamer plugin '{}' not found", needed[i]);
      ret = false;
      continue;
    }
    gst_object_unref(plugin);
  }
  return ret;
}

} // namespace quiddities
} // namespace switcher
