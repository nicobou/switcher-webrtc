#!/usr/bin/env python3

# from http.server import HTTPServer, SimpleHTTPRequestHandler
# import ssl

# httpd = HTTPServer(("", 8000), SimpleHTTPRequestHandler)

# httpd.socket = ssl.SSLContext.wrap_socket(httpd.socket,
#                                           keyfile="key.pem",
#                                           certfile='cert.pem',
#                                           server_side=True)

# httpd.serve_forever()

import http.server
import ssl
import socketserver

context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
context.load_cert_chain("cert.pem", keyfile="key.pem")
server_address = ("127.0.0.1", 8090)
handler = http.server.SimpleHTTPRequestHandler
with socketserver.TCPServer(server_address, handler) as httpd:
    httpd.socket = context.wrap_socket(httpd.socket, server_side=True)
    httpd.serve_forever()
